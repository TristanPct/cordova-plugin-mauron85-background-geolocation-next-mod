/*
According to apache license

This is fork of christocracy cordova-plugin-background-geolocation plugin
https://github.com/christocracy/cordova-plugin-background-geolocation

This is a new class based on atishtechage code (https://github.com/atishtechage/backgroundgps/blob/master/GooglePlayServices/src/com/google/android/googleplayservices/LocationWebService.java)
*/

package com.marianhello.cordova.bgloc;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import android.os.AsyncTask;
import android.util.Log;

public class LocationWebService extends AsyncTask<String, String, Boolean> {
    private static final String TAG = "LocationWebService";

    public LocationWebService() {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected Boolean doInBackground(String... arg0) {
        if (arg0[0] == null || arg0[0].isEmpty()) {
            return null;
        }

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(arg0[0]);
            httppost.setHeader("Content-Type", "application/json");
            if (arg0[2] != null && !arg0[2].isEmpty()) {
                httppost.setHeader("Authorization", arg0[2]);
            }
            HttpParams httpParameters = new BasicHttpParams();

            httpclient = new DefaultHttpClient(httpParameters);

            httppost.setEntity(new StringEntity(arg0[1]));

            HttpResponse response;
            response = httpclient.execute(httppost);
            Log.d(TAG, "Sended to " + arg0[0] + " : " + arg0[1]);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                Log.d(TAG, "Server responded " + statusLine.getStatusCode() + " " + statusLine.getReasonPhrase());
            } else {
                Log.e(TAG, "Server responded " + statusLine.getStatusCode() + " " + statusLine.getReasonPhrase());
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
